# Import libraries for simulation
import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt


# Imports for visualization
import PIL.Image
from io import BytesIO
import time


def draw_mandelbrot_np(x_v=(-2, 1), y_v=(-1, 1), screen_size=(1024, 768), n_iters=1024):
    """ y_min, y_max = y_v
        x_min, x_max = x_v
        
        screen_size=(nw, nh)"""

    assert x_v[1] > x_v[0], 'ERROR: x_v[1] <= x_v[0]'
    assert y_v[1] > y_v[0], 'ERROR: y_v[1] <= y_v[0]'
    
    x_min, x_max = np.float128(x_v[0]), np.float128(x_v[1])
    y_min, y_max = np.float128(y_v[0]), np.float128(y_v[1])
    
    dx = (x_max - x_min) / np.float128(screen_size[0])
    dy = (y_max - y_min) / np.float128(screen_size[1])
    
    X, Y = np.meshgrid(np.arange(x_min, x_max, dx, np.float128), np.arange(y_min, y_max, dy, np.float128))

    C = np.array(X + 1j * Y[::-1], dtype=np.complex128)
    Z = C
    n = np.zeros_like(C, np.float32)

    for i in range(n_iters):
        Z = Z * Z + C
        n += (np.abs(Z) < 2).astype(np.float32)

    n = n / n_iters
    return n



def draw_mandelbrot_tf2(x_v=(-2, 1), y_v=(-1, 1), screen_size=(1024, 768), n_iters=1024):
    """ y_min, y_max = y_v
        x_min, x_max = x_v
        
        screen_size=(nw, nh)"""

    assert x_v[1] > x_v[0], 'ERROR: x_v[1] <= x_v[0]'
    assert y_v[1] > y_v[0], 'ERROR: y_v[1] <= y_v[0]'
    
    x_min, x_max = np.float128(x_v[0]), np.float128(x_v[1])
    y_min, y_max = np.float128(y_v[0]), np.float128(y_v[1])
    
    dx = (x_max - x_min) / np.float128(screen_size[0])
    dy = (y_max - y_min) / np.float128(screen_size[1])
    
    X, Y = np.meshgrid(np.arange(x_min, x_max, dx, np.float128), np.arange(y_min, y_max, dy, np.float128))

    C = tf.constant(X + 1j * Y[::-1], dtype=tf.complex128)
    Z = C
    n = tf.zeros_like(C, tf.float32)

    for i in range(n_iters):
        Z = Z * Z + C
##        n += tf.cast( ( tf.real(Z * tf.conj(Z)) < 4), tf.float32 )
        n += tf.cast( (tf.abs(Z) < 2), tf.float32 )

    n = n / n_iters
    return n.numpy()

def draw_mandelbrot_tf(sess=None, x_v=(-2, 1), y_v=(-1, 1), screen_size=(1024, 768), n_iters=1024):
    """ y_min, y_max = y_v
        x_min, x_max = x_v
        
        screen_size=(nw, nh)"""

    assert x_v[1] > x_v[0], 'ERROR: x_v[1] <= x_v[0]'
    assert y_v[1] > y_v[0], 'ERROR: y_v[1] <= y_v[0]'
    
    x_min, x_max = np.float128(x_v[0]), np.float128(x_v[1])
    y_min, y_max = np.float128(y_v[0]), np.float128(y_v[1])
    
    dx = (x_max - x_min) / np.float128(screen_size[0])
    dy = (y_max - y_min) / np.float128(screen_size[1])

    X, Y = np.meshgrid(np.arange(x_min, x_max, dx, np.float128), np.arange(y_min, y_max, dy, np.float128))

    if sess == None:
        sess = tf.compat.v1.InteractiveSession()

    Z = X + 1j*Y[::-1]

    xs = tf.constant(Z.astype(np.complex128))
    zs = tf.Variable(xs)
    ns = tf.Variable(tf.zeros_like(xs, tf.float32))

    tf.global_variables_initializer().run()

    
    for i in range(n_iters):
        zs = zs*zs + xs
        not_diverged = tf.abs(zs) < 2
        ns += tf.cast(not_diverged, tf.float32)
    
    n = ns / n_iters
    return n.eval()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def get_graph(screen_size=(1024, 768), n_iters=100):

    C = tf.compat.v1.placeholder(tf.complex128, screen_size[::-1])
    Z = C
    n = tf.zeros_like(C, tf.float32)
    
    for i in range(n_iters):
        Z = Z * Z + C
        n += tf.cast( (tf.abs(Z) < 2), tf.float32 )

    n = n / n_iters
    
    return C, n
        
    


def draw_mandelbrot_tf1(sess, C, N,  x_v=(-2, 1), y_v=(-1, 1), screen_size=(1024, 768)):
    """ y_min, y_max = y_v
        x_min, x_max = x_v
        
        screen_size=(nw, nh)"""

    assert x_v[1] > x_v[0], 'ERROR: x_v[1] <= x_v[0]'
    assert y_v[1] > y_v[0], 'ERROR: y_v[1] <= y_v[0]'
    
    x_min, x_max = np.float128(x_v[0]), np.float128(x_v[1])
    y_min, y_max = np.float128(y_v[0]), np.float128(y_v[1])
    
    dx = (x_max - x_min) / np.float128(screen_size[0])
    dy = (y_max - y_min) / np.float128(screen_size[1])
    
    X, Y = np.meshgrid(np.arange(x_min, x_max, dx, np.float128), np.arange(y_min, y_max, dy, np.float128))

    C_np = X + 1j * Y[::-1]
    
    n = sess.run(N, feed_dict={C: C_np})
    
    return n


def coords_parser(coords_str='22222222222222c200c00000f5070080 5c8fc2f5285c8fc2fd3f0000f5070080 935f2cf9c5925facfdbf00005f7f0000 184b7eb1e4174b8e004000005f7f0000'):
    coords_v = coords_str.split()
    coords_float_v = [np.frombuffer(bytes.fromhex(c_str), np.float128)[0]  for c_str in coords_v]

    assert len(coords_float_v) == 4, 'Must by four!!!'

    x_v = coords_float_v[:2]
    y_v = coords_float_v[2:]
    return x_v, y_v
        
    
    

if __name__ == '__main__':
    sess = None
    eager_enabled = False

    screen_size = (1024, 768)
    n_iters = 2000
    C, N = None, None

    coords_str = '30e9e3527a97d4f1fcbf0000fb070000 9a16ce527a97d4f1fcbf0000fb070000 636efe228345beb9fe3f0000b17f0000 df8502238345beb9fe3f0000b17f0000'

    x_v, y_v =(-2, 1), (-1, 1)
    x_v, y_v = coords_parser(coords_str)
    
    if 0:
        t_start = time.time()
        n = draw_mandelbrot_np(x_v=x_v, y_v=y_v, screen_size=screen_size, n_iters=n_iters)
        print('Implementación "np", finaliza en: {0:0.02f} (s)'.format(time.time()-t_start))


    if 0:
        if sess is None:
            sess = tf.compat.v1.InteractiveSession()
        t_start = time.time()
        n = draw_mandelbrot_tf(sess, x_v=x_v, y_v=y_v, screen_size=screen_size, n_iters=n_iters)
        print('Implementación "tf", finaliza en: {0:0.02f} (s)'.format(time.time()-t_start))
    
    if 0:
        if not eager_enabled:
            tf.compat.v1.enable_eager_execution()
            eager_enabled = True
        
        t_start = time.time()
        n = draw_mandelbrot_tf2(x_v=x_v, y_v=y_v, screen_size=screen_size, n_iters=n_iters)
        print('Implementación "tf2", finaliza en: {0:0.02f} (s)'.format(time.time()-t_start))


    if 1:
        if sess is None:
            sess = tf.compat.v1.Session()

        if (C, N) == (None, None):
            C, N = get_graph(screen_size=screen_size, n_iters=n_iters)
        
        t_start = time.time()
        n = draw_mandelbrot_tf1(sess, C, N,  x_v=x_v, y_v=y_v, screen_size=screen_size)
        print('Implementación "tf1", finaliza en: {0:0.02f} (s)'.format(time.time()-t_start))

        
    plt.imshow(n, cmap='jet')
    plt.show()
    
        
    


