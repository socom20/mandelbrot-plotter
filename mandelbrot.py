import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib as mpl
import time

import pygame as pg
import pygame.locals
import pygame.math as pm
import pygame.freetype

from PIL import Image
from Mandelbrot_shot import *
import random
import os, sys
import cv2



class MandelBroutPlotter():
    def __init__(self, mandelbrot_implementation='tf2', screen_size=(1024, 768)):

        self.screen_size          = screen_size # tamaño de la pantalla
        self.mandelbrot_implementation = mandelbrot_implementation


        if self.mandelbrot_implementation == 'tf2':
            if int(tf.version.VERSION.split('.')[0]) == 1:
                tf.compat.v1.enable_eager_execution()
        elif self.mandelbrot_implementation == 'tf':
            if int(tf.version.VERSION.split('.')[0]) > 1:
                tf.compat.v1.disable_eager_execution()
            self.sess = tf.compat.v1.InteractiveSession()
        elif self.mandelbrot_implementation == 'tf1':
            if int(tf.version.VERSION.split('.')[0]) > 1:
                tf.compat.v1.disable_eager_execution()
            self.sess = tf.compat.v1.Session()
        elif self.mandelbrot_implementation == 'np':
            pass
        else:
            raise Exception('Implementation ERROR.')

        
        self.monitor_size         = None
        self.global_pos           = pm.Vector2(self.screen_size[0]//2, -self.screen_size[1]//2) # coord canvas    
        self.global_zoom_factor   = np.float128(300) # coord screen
        self.global_zoomf_min     = 100.0
        self.global_zoomf_max     = 50e15
        self.screen_mouse_pos     = pm.Vector2(0,0) # coord screen
        
        self.canvas_mouse_pos     = self.coord_screen2coord_canvas(self.screen_mouse_pos) # coord canvas

        self.iterations = 100

        self.cmap_v = ['jet', 'viridis', 'plasma', 'inferno', 'Oranges', 'hsv', 'gist_rainbow', 'rainbow', 'gnuplot', 'nipy_spectral', 'gist_ncar', 'hot', 'brg']
        self.cmap_idx = 0
        self.change_cmap(self.cmap_idx)
        
        
        self.global_bg_color   = (14, 17, 17)

        self.text_color = (128,128,128)

        self.fullScreen = False
        self.screen = self.init_display(display_size=self.screen_size, fullScreen=self.fullScreen)

##        self.myfont = pg.freetype.Font("./fonts/BebasNeue-Regular.ttf", 24) # fuente elegida para el texto
        self.myfont = pg.freetype.Font("./fonts/Helvetica.ttf", 10) # fuente elegida para el texto

        self.is_running = True


        self.do_save = False
        self.make_video = False

        
        
        
        return None


    def change_cmap(self, idx=None):
        if idx is None or idx > len(self.cmap_v) or idx < 0:
            self.cmap_idx = (self.cmap_idx+1) % len(self.cmap_v)
            
        else:
            self.cmap_idx  = idx
            
        self.current_cmap = self.cmap_v[self.cmap_idx]
        self.cmap = mpl.cm.get_cmap(self.current_cmap)
        return None

    def video_rec(self):
        self.video_output_path = './video.avi'

        if not self.make_video:
            self.video_size = tuple([int(a) for a in self.screen_size])
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            self.video_out = cv2.VideoWriter(self.video_output_path, fourcc, 30.0, self.video_size)

            print('Empezando nuevo video:', self.video_output_path)
        else:
            self.video_out.release()
            print('Video finalizado')

            
        self.make_video = not self.make_video
            
        return None

    def update_tf_graph(self):
        self.C, self.N = get_graph(screen_size=self.screen_size, n_iters=self.iterations)
        return None

    def coord_screen2coord_canvas(self, screen_pos = (0,0)):
        """Transformo una posición de coordenadas SCREEN en una posicion de coordenadas CANVAS"""
        screen_pos = np.array([screen_pos[0], -screen_pos[1]], dtype=np.float128)
        canvas_pos = (screen_pos - np.float128(self.global_pos)) / np.float128(self.global_zoom_factor)
        return canvas_pos


    def coord_canvas2coord_screen(self, canvas_pos = (0,0)):
        """Transformo una posición de coordenadas CANVAS en una posicion de coordenadas SCREEN"""
        canvas_pos = pm.Vector2(canvas_pos)
        screen_pos = canvas_pos * self.global_zoom_factor + self.global_pos

        screen_pos = pm.Vector2(int(round(screen_pos.x)), -int(round(screen_pos.y)))
        return screen_pos

    def drag_view(self):
        """Desplazo la vista en la pantalla"""
        self.global_pos += pm.Vector2(self.screen_mouse_dx, -self.screen_mouse_dy)
        return None

    
    def init_display(self, display_size=(0,0), screen_name="Mandelbrot Plotter", fullScreen = False):
        """ Inicializa pygame y crea un onjeto screen.
            display_size: tamaño de la ventana de pygame.
            screen_name: Nombre que aparecerá en la ventana abierta
            fullScreen: Modo pantalla completa. Inicia en Falso
            return: screen"""

        pg.init()
        display_info = pg.display.Info()
        window_size  = (display_info.current_w, display_info.current_h)
        
        if self.monitor_size is None:
            self.monitor_size = window_size
            
        self.clock = pg.time.Clock()
        
        if fullScreen:
            screen = pg.display.set_mode(self.monitor_size, pg.HWSURFACE | pg.FULLSCREEN)
            
        else:
            screen = pg.display.set_mode(display_size, pygame.locals.HWSURFACE | pygame.locals.DOUBLEBUF | pygame.locals.RESIZABLE)
            
        pg.display.set_caption(screen_name)

        if self.mandelbrot_implementation == 'tf1':
            self.update_tf_graph()

        return screen

    def fullScreen_Mode(self):
        """Si "fullScreen is True" activa el modo pantalla completa"""
        self.screen = self.init_display(fullScreen=self.fullScreen)
        return None


    def update_stats(self):
        pos = pm.Vector2(0, 0)

        if  self.global_zoom_factor < 1e3:
            zoom_str = '{} : x{:}'.format('Zoom factor',int(self.global_zoom_factor))
            
        elif self.global_zoom_factor < 1e6:
            zoom_str = '{} : x{:}k'.format('Zoom factor',int(self.global_zoom_factor/1e3))

        elif self.global_zoom_factor < 1e9:
            zoom_str = '{} : x{:}M'.format('Zoom factor',int(self.global_zoom_factor/1e6))

        elif self.global_zoom_factor < 1e12:
            zoom_str = '{} : x{:}G'.format('Zoom factor',int(self.global_zoom_factor/1e9))

        elif self.global_zoom_factor < 1e15:
            zoom_str = '{} : x{:}T'.format('Zoom factor',int(self.global_zoom_factor/1e12))
            
        else:
            zoom_str = '{} : x{:}P'.format('Zoom factor',int(self.global_zoom_factor/1e15))

            
        text_v = [
            '{} : {}'.format('Implementation', self.mandelbrot_implementation),
            zoom_str,
            '{} : {:0.2f}'.format('Fps', self.clock.get_fps()),
            '{} : {}'.format('Canvas Mouse Pos', self.canvas_mouse_pos),
            '{} : {}'.format('Iterations', self.iterations),
            '{} : {}'.format('ColorMap', self.current_cmap),
            '{} : {}'.format('VideoRec', self.make_video),
            
##            '{} : {}'.format('Screen Mouse Pos', self.screen_mouse_pos),
##            '{} : {}'.format('Canvas Global Pos', self.global_pos),
            
                  ]

        size = 20
        for i, text in enumerate(text_v):
            text_surface, rect = self.myfont.render(text, self.text_color, size=size)
            self.screen.blit(text_surface, pos+pm.Vector2(0, (size+2)*i))
            
        return None

    def update_mandelbrot(self):
        x_min, y_max = self.coord_screen2coord_canvas( (0.0, 0.0) )
        x_max, y_min = self.coord_screen2coord_canvas( self.screen_size )
        
        
        if self.mandelbrot_implementation == 'tf2':
            img_mandelbot = draw_mandelbrot_tf2(x_v=(x_min,x_max), y_v=(y_min,y_max), screen_size=self.screen_size, n_iters=self.iterations)
            
        elif self.mandelbrot_implementation == 'tf':
            img_mandelbot = draw_mandelbrot_tf(self.sess, x_v=(x_min,x_max), y_v=(y_min,y_max), screen_size=self.screen_size, n_iters=self.iterations)
            
        elif self.mandelbrot_implementation == 'tf1':
            img_mandelbot = draw_mandelbrot_tf1(self.sess, self.C, self.N, x_v=(x_min,x_max), y_v=(y_min,y_max), screen_size=self.screen_size)

        elif self.mandelbrot_implementation == 'np':
            img_mandelbot = draw_mandelbrot_np(x_v=(x_min,x_max), y_v=(y_min,y_max), screen_size=self.screen_size, n_iters=self.iterations)
        else:
            raise Exception('Implementation ERROR: ')
    
        img = ( 255*self.cmap( img_mandelbot.T ) )[...,:3].astype(np.uint8)

        if self.make_video:
            frame = cv2.resize(img.transpose([1,0,2]), self.video_size)
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR) 
            self.video_out.write(frame)
            
            
        if self.do_save:
            cap_dir = './captures'
            self.do_save = False
            if not os.path.exists(cap_dir):
                os.makedirs(cap_dir)
            
            save_path = os.path.join(cap_dir, 'saved_{}.png'.format(''.join(random.choices('0123456789abcdef', k=3))))
            log_path = os.path.join(cap_dir, 'log.txt')
            save_coord = 'saved: "{}"   Iters: {}  Coords: {} {} {} {}\n'.format(
                save_path,
                self.iterations,
                x_min.tobytes().hex(),
                x_max.tobytes().hex(),
                y_min.tobytes().hex(),
                y_max.tobytes().hex(),
                )
            
            with open(log_path, 'a') as f:
                f.write(save_coord)

            pil_img = Image.fromarray(img.transpose([1,0,2]))
            pil_img.save(save_path)
            
            print(f' Salvando imagen: {save_path}  coordenadas en:{log_path}')
        
        surf = pg.surfarray.make_surface(img)
        
        blit_pos = pm.Vector2(0, 0)
        self.screen.blit(surf, blit_pos)
        
        return None
    

    def update_display(self):
        """ Actualiza los objetos que se dibujan en Screen.   
            return: None"""

        self.clock_tick  = self.clock.tick(60)
        
        self.screen.fill(self.global_bg_color)
        
        self.update_mandelbrot()

        self.update_stats()

        pg.display.flip()
        
        return None



    def main_loop(self):
        """Loop principal"""

        while self.is_running:

            self.update_display()
            self.event_handler()


        pg.quit()
        pg.display.quit()

        return None

    def zoom_in(self):
        """Acerco la vista en la pantalla"""
        if self.global_zoom_factor <= self.global_zoomf_max:
            self.global_zoom_factor = self.global_zoom_factor * np.float128(1.25)
            self.global_pos += pm.Vector2(self.global_pos.x-self.screen_mouse_pos[0], self.global_pos.y+self.screen_mouse_pos[1])*(1.25-1)

        return None

    def zoom_out(self):
        """Alejo la vista en la pantalla"""
        if self.global_zoom_factor >= self.global_zoomf_min:
            self.global_zoom_factor = self.global_zoom_factor * np.float128(0.8)
            self.global_pos += pm.Vector2(self.global_pos.x-self.screen_mouse_pos[0], self.global_pos.y+self.screen_mouse_pos[1])*(0.8-1)

        return None

    def update_mouse_pos(self, pos):
        """Actualizo la posición del mouse en la pantalla"""
        self.screen_mouse_dx = pos[0] - self.screen_mouse_pos[0]
        self.screen_mouse_dy = pos[1] - self.screen_mouse_pos[1]
        self.screen_mouse_pos = pos

        self.canvas_mouse_pos = self.coord_screen2coord_canvas(self.screen_mouse_pos)
        return None
    

    def quit(self):
        """Se cierra Pygame"""
        self.is_running = False
        return None

    def save(self):
        self.do_save = True
        return None

    def more_iterations(self, delta_iter=50):
        if self.iterations < 2000:
            self.iterations += abs(delta_iter)
            if self.mandelbrot_implementation == 'tf1':
                self.update_tf_graph()
        else:
            print('No more!!!')
        return None

    def less_iterations(self, delta_iter=50):
        if self.iterations > 50:
            self.iterations -= abs(delta_iter)
            if self.mandelbrot_implementation == 'tf1':
                self.update_tf_graph()
        else:
            print('No less!!!')
            
        return None
    
    def event_handler(self):
        """Lista de comandos para manejar los eventos desde teclado y mouse"""
        keys_pressed_v = pg.key.get_pressed()
        events_v       = pg.event.get()

        pressed_ctrl  = (keys_pressed_v[pg.K_LCTRL] or keys_pressed_v[pg.K_RCTRL])
        pressed_shift = (keys_pressed_v[pg.K_LSHIFT] or keys_pressed_v[pg.K_RSHIFT])
        pressed_alt   = (keys_pressed_v[pg.K_LALT] or keys_pressed_v[pg.K_RALT])
        for event in events_v:
            
            if event.type == pg.QUIT:
                self.quit()
                break
                        
            # PRESIONO UN BOTON DEL MOUSE
            elif event.type == pg.MOUSEBUTTONDOWN:
                
                if event.button == 4: # ruedita del mouse hacia arriba
                    # zoom in
                    self.zoom_in()

                elif event.button == 5: # ruedita del mouse hacia abajo
                    # zoom out
                    self.zoom_out()
                
            elif event.type == pg.VIDEORESIZE and not self.fullScreen:
               screen_size = event.dict['size']
               self.screen = self.init_display(display_size = screen_size)

                    
            # MUEVO EL MOUSE
            elif event.type == pg.MOUSEMOTION:
                self.update_mouse_pos(event.pos)
                
                mouseState = pg.mouse.get_pressed()
                if mouseState[1]:  # Ruedita apretada
                    # arrastro lo que se muestra por pantalla
                    self.drag_view()


            # PRESIONO UNA TECLA
            elif event.type ==  pg.KEYDOWN:
                
                # guardo en un archivo de texto            
                if event.key == pg.K_s and pressed_ctrl:
                    self.save()

                elif event.key == pg.K_v and pressed_ctrl:
                    self.video_rec()

                elif event.key == pg.K_c and pressed_ctrl:
                    self.change_cmap()
                    
                # se incrementa en 1 el nro de nodos del pipe seleccionado
                elif (event.key == pg.K_KP_PLUS):
                    self.more_iterations()
                                    
                # se disminuye en 1 el nro de nodos del pipe seleccionado
                elif (event.key == pg.K_KP_MINUS):
                    self.less_iterations()
                            
                        
                            
                
        return None

    
if __name__ == '__main__':
    ptr = MandelBroutPlotter()
    ptr.main_loop()
    
    if 0:
        t0 = time.time()
        n = draw_mandelbrot_tf2(n_iters=1024)
        dt = (time.time() - t0)
        print(f' - frame time = {int(1000*dt)} ms  -  fps = {int(1/dt)}')

##    plt.imshow(n)
##    plt.show()
    
